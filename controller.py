#!/usr/bin/python3
#
# PressController v0.1
# Todd Groff 2018

import Adafruit_GPIO.GPIO as GPIO
import Adafruit_GPIO.SPI as spidev
import Adafruit_MAX31855.MAX31855 as MAX31855

import time
import threading
import sys
import math
import signal


class PressController:

    # Button press timestamps 
    t1 = 0
    t2 = 0   
    def __init__(self, a=True):
        self.run = False
        self.timestep = 0.01 # Time step for main loop
        self.T_setpoint = 30 # Default target temp in Celsius
        self.anti_tiedown_time  = 0.1 # Maximum time between button presses for activation
        self.keep_down_time = 10 # How long the solenoid remains active

        # Setup thread to run control loop in parallel 
        self.loop_thread = threading.Thread(target=self.loop)
        
        # Pin Definitions
        self.pin_SW1 = 24
        self.pin_SW2 = 25
        self.pin_H1 = 27
        self.pin_H2 = 18
        self.pin_Solenoid = 17

        # Initialize Switches
        self.SW1 = Button(self.pin_SW1)
        self.SW2 = Button(self.pin_SW2)
        
        # Initialize temp controllers (heater pin, SPI device)
        self.TC1 = TempController(self.pin_H1, spidev.SpiDev(0,0), self.T_setpoint)
        self.TC2 = TempController(self.pin_H2, spidev.SpiDev(0,1), self.T_setpoint)

        # Initialize solenoid
        self.solenoid = Solenoid(self.pin_Solenoid)

    # Main control loop
    def loop(self):
        while(self.run): 
            # Update switch states
            self.SW1.update()
            self.SW2.update()
            #print("SW1: %r, SW2: %r"%(self.SW1.pressed(),self.SW2.pressed()))

            # Update temp controllers
            self.TC1.update()
            self.TC2.update()
            print("Temp 1: %.2fC, Temp 2: %.2fC, Ambient: %.2fC"%(self.TC1.temp, self.TC2.temp, self.TC2.thermocouple.readInternalC()))
            #print(bin(self.TC1.thermocouple._read32()))
            # Check if buttons are pressed
            if self.SW1.rising():
                self.t1 = time.time() # grab timestamp           
                if math.fabs(self.t1 - self.t2) < self.anti_tiedown_time: # check delta T agains anti tiedown time
                        self.solenoid.forward()
                        #press_thread = threading.Thread(target=self.press)
                        #press_thread.start() # Run press function in parallel
                        #continue # skip the rest of this loop iteration

            if self.SW2.rising():
                self.t2 = time.time()
                if math.fabs(self.t1 - self.t2) < self.anti_tiedown_time:
                        self.solenoid.forward()
                        #press_thread = threading.Thread(target=self.press)
                        #press_thread.start()
            
            if not self.SW1.pressed() or not self.SW2.pressed(): self.solenoid.reverse()

            time.sleep(self.timestep)
    

    def start(self):
        self.run = True
        self.TC1.setTemp(40)
        self.TC2.setTemp(40)
        self.loop_thread.start()

    def stop(self):

        # Turn off temp controllers
        self.TC1.setTemp(0)
        self.TC1.update()

        self.TC2.setTemp(0)
        self.TC2.update()

        self.run = False
        
    def press(self):
        print("Pressing... dT: %f"%(math.fabs(self.t1-self.t2)))
        self.solenoid.forward()
        time.sleep(self.keep_down_time)
        self.solenoid.reverse()

class Button:
    def __init__(self, pin, pullup=True):
        self.pin = pin
        self.prev = False
        self.curr = False
        gpio.setup(pin, GPIO.IN, pull_up_down=(GPIO.PUD_UP if pullup else GPIO.PUD_DOWN))

    def update(self):
        self.prev = self.curr
        self.curr = not gpio.input(self.pin)

    def pressed(self): return self.curr
    def rising(self): return self.curr and not self.prev
    def falling(self): return not self.curr and self.prev

class TempController:
    def __init__(self, pin_relay, spi, T_init=0, deadband=1, T_max=80):
        self.pin_relay = pin_relay
        self.setpoint = T_init
        self.deadband = deadband
        self.T_max = T_max

	# Setup relay pin
        gpio.setup(pin_relay, GPIO.OUT)
        gpio.output(pin_relay, False)

        # Initialize thermocouple
        self.thermocouple = MAX31855.MAX31855(spi=spi)

    def update(self):
        self.temp = self.thermocouple.readTempC()
        self.tempF = (self.temp+40)*1.8-40

        # Turn on heaters
        if self.temp < self.setpoint - self.deadband/2: gpio.output(self.pin_relay, False)

        # Turn off heaters
        if self.temp > self.T_max or self.temp > self.setpoint + self.deadband/2: gpio.output(self.pin_relay, True)

    def setTemp(self, setpoint):
        self.setpoint = setpoint

    def setDeadband(self, deadband):
        self.deadband = deadband

class Solenoid:
    def __init__(self, pin):
        self.pin = pin
        gpio.setup(pin, GPIO.OUT)
        self.reverse()

    def forward(self):
        gpio.output(self.pin, False)

    def reverse(self):
        gpio.output(self.pin, True)

def exit(signal, frame):
    print("Stopping Controller...")
    controller.stop()
    sys.exit(0)

# Only run if executed as a script and not imported
if __name__ == "__main__":
    gpio = GPIO.get_platform_gpio() # Get GPIO Object specific to the Pi 3
    controller = PressController()

    signal.signal(signal.SIGINT, exit)
    signal.signal(signal.SIGTERM, exit)

    controller.start()
    while True: time.sleep(0.5) # Empty while loop to keep the script alive
     

